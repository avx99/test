 HttpHost targetHost = new HttpHost("localhost", 8001, "http");
    DefaultHttpClient httpclient = new DefaultHttpClient();

    final String userName = "admin";
    final String password = "password";

    httpclient.getCredentialsProvider().setCredentials(
            new AuthScope("localhost", 8001), 
            new UsernamePasswordCredentials(userName, password));   


    // Create AuthCache instance
    AuthCache authCache = new BasicAuthCache();
    // Generate DIGEST scheme object, initialize it and add it to the local
    // auth cache
    DigestScheme digestAuth = new DigestScheme();
    // Suppose we already know the realm name
    digestAuth.overrideParamter("realm", "some realm");
    // Suppose we already know the expected nonce value
    digestAuth.overrideParamter("nonce", "whatever");
    authCache.put(targetHost, digestAuth);

    // Add AuthCache to the execution context
    BasicHttpContext localcontext = new BasicHttpContext();
    localcontext.setAttribute(ClientContext.AUTH_CACHE, authCache);

    HttpGet httpget = new HttpGet("http://localhost:8001/rest/test");

    try {
        HttpResponse response = httpclient.execute(targetHost, httpget, localcontext);
        HttpEntity entity = response.getEntity();

        System.out.println("----------------------------------------");
        System.out.println(response.getStatusLine());
        if (entity != null) {
            System.out.println("Response content length: " + entity.getContentLength());
        }
        EntityUtils.consume(entity);
    } finally {
        httpclient.getConnectionManager().shutdown();
    }
}